open Definitions;;

exception UnifyException of term * term;;

let rec apply (t : term) (unifier : (int * term) list) =
  match t with
  | Fun (f, n, l) ->
     let apply' t = apply t unifier in
     Fun (f, n, List.map apply' l)
  | Val v -> t
  | Var i ->
     try (fun (_, t) -> t) (List.find (fun ((j, _) : (int * term)) -> i = j) unifier)
     with Not_found -> t;;

let rec apply_to_body (unifier : (int * term) list) (list : (pred * args) list) =
  match list with
  | [] -> []
  | (p, args) :: rest -> (p, List.map (fun t -> apply t unifier) args) :: (apply_to_body unifier rest);;
        

let combine_option u1 u_option =
  let comb u1 u2 =
    List.map (fun (i, t) -> (i, apply t u2)) u1
  in
  let rec diff u1 u2 =
    match u2 with
    | [] -> []
    | (i, t) :: rest ->
       if List.exists (fun ((j, _) : (int * term)) -> i = j) u1
       then
       diff u1 rest
       else
       (i, t) :: diff u1 rest
  in
  match u_option with
  | None -> None
  | Some u2 ->
     Some (List.append (comb u1 u2) (diff u1 u2))

let rec mgu_combine (l : (int * term) list list) =
  let combine u1 u2 =
    let comb u1 u2 = u1 in
    let rec diff u1 u2 =
      match u2 with
      | [] -> []
      | (i, t) :: rest ->
         if List.exists (fun ((j, _) : (int * term)) -> i = j) u1
         then
           diff u1 rest
         else
           (i, t) :: diff u1 rest
    in
    List.append (comb u1 u2) (diff u1 u2)
  in
  match l with
  | [] -> []
  | [x] -> x
  | x :: y :: xs -> mgu_combine ((combine x y) :: xs);;
  
let unify (t1 : term) (t2 : term) =
  let rec unify_terms (t1, t2) = 
    match (t1, t2) with
    | (Var i, t2) -> [(i, t2)]
    | (t1, Var j) -> [(j, t1)]
    | (Val i, Val j) -> if i = j then [] else raise (UnifyException (t1, t2))
    | (Fun (f, n, a1), Fun (g, m, a2)) ->
       if (String.compare f g) != 0 || n != m then
         raise (UnifyException (t1, t2))
       else
         let comb = List.combine a1 a2 in
         let compute_mgu old_mgu (a, b) =
           mgu_combine [old_mgu; unify_terms (apply a old_mgu, apply b old_mgu)]
         in
         List.fold_left compute_mgu [] comb
    | _ -> raise (UnifyException (t1, t2))
  in
  unify_terms (t1, t2);;

let unify_args (args1 : args) (args2 : args) =
  let n = List.length args1 in
  let m = List.length args2 in
  try
  Some (unify (Fun ("", n, args1)) (Fun ("", m, args2)))
  with
  | UnifyException (_, _) -> None
               

(*
 * a small prolog program
 * p(a).
 * q(b).
 * q(X) <- p(X).
 * will be transated into the following program: 
 *)

let (p : pred) = { name = "p"; arity =  1; clauses = [] };;
let (q : pred) = { name = "q"; arity =  1; clauses = [] };;
let (is_list : pred) = { name = "is_list"; arity = 1; clauses = [] };;
let (is_list_1 : clause) = ([Val (Str "nil")], []);;
let (is_list_2 : clause) = ([Fun ("cons", 2, [Var 1; Var 2])], [(is_list, [Var 2])]);;
let (p1 : clause) = ([Val (Chr 'a')], []);;
let (q1 : clause) = ([Val (Chr 'b')], []);;
let (q2 : clause) = ([Var 1], [(p, [Var 1])]);;
p.clauses <- [p1];;
q.clauses <- [q2; q1];;
is_list.clauses <- [is_list_1; is_list_2];;
let (prog : program) = Hashtbl.create 2;;
Hashtbl.add prog ("p", 1) p;;
Hashtbl.add prog ("q", 1) q;;
Hashtbl.add prog ("is_list", 1) is_list;;
let (names : (int, string) Hashtbl.t) = Hashtbl.create 2;;
Hashtbl.add names ~-1 "X";;

(* e.g.: find_answer prog p [Var 1] *)

let internal_variable_counter =
  let counter = ref (-1) in
  fun () -> begin
  incr counter;
  !counter
  end;;

let clause_new_vars ((args, lst) : clause) =
  let rec new_vars_term db term =
    match term with
    | Val i -> term
    | Var v ->
       begin
       try
       let new_v = Hashtbl.find db v in
       Var new_v
       with
       | Not_found ->
          let new_v = (internal_variable_counter ()) in
          Hashtbl.add db v new_v;
          Var new_v
       end
    | Fun (n, a, list) ->
       Fun (n, a, new_vars db list)
    and
      new_vars db args =
      match args with
      | [] -> []
      | t :: rest ->
         (new_vars_term db t) :: (new_vars db rest)
  in
    let db = Hashtbl.create 1024 in
    (new_vars db args, List.map (fun (p, args) -> (p, new_vars db args)) lst);;

let predicate_new_vars p =
  { p with clauses = List.map clause_new_vars p.clauses };;
  

let rec print_term (t : term) (var_names : (int, string) Hashtbl.t) =
  let rec print_term_list (ts : term list) =
    match ts with
    | [] -> ()
    | [t] -> print_term t var_names
    | t :: rest ->
       print_term t var_names;
       print_char ',';
       print_term_list rest
  in                  
  let print_value (v : value) =
    match v with
    | Int i ->
       print_int i
    | Str s ->
       print_string s
    | Chr c ->
       print_char c
  in
  match t with
  | Var i ->
     begin
     try
     let name = Hashtbl.find var_names i in
     print_string name
     with
     | Not_found ->
        print_string "v$";
        print_int i
     end
  | Val v ->
     print_value v
  | Fun (name, arity, args) ->
     print_string name;
     print_char '(';
     print_term_list args;
     print_char ')';;

exception No_solution;;

let proof ?(on_success=fun _ -> true) (p : pred) (input_args : args) =
  let rec mgu_project mgu args =
    let rec search_for_var i term =
      match term with
      | Val v -> false
      | Var j -> i = j
      | Fun (_, _, args) ->
         List.exists (search_for_var i) args
    in
    match mgu with
    | [] -> []
    | (i, t) :: rest ->
       if List.exists (search_for_var i) args
       then (i, t) :: (mgu_project rest args)
       else mgu_project rest args
  in
  let rec proof_clauses (clauses : clause list) (args : args) =
    match clauses with
    | [] -> None
    | clause :: rest ->
       let gu = begin
       let mgu = unify_args args (fst clause) in
       match mgu with
       | None -> None
       | Some uni ->
          if (snd clause) = []
          then if on_success (mgu_project uni input_args)
               then mgu
               else None
          else let compl = List.fold_left (fun mgu (p, args) ->
                           match mgu with
                           | None -> None
                           | Some unifier ->
                              List.map (fun t -> apply t unifier) args
                              |> proof_predicate (predicate_new_vars p)
                              |> combine_option unifier) mgu (snd clause)
               in
               (* only return the asked for variables *)
               match compl with
               | None -> None
               | Some uni ->
                  Some (mgu_project uni args)
       end
       in
       match gu with
       | None -> proof_clauses rest args
       | Some _ -> gu
  and proof_predicate ({name;arity;clauses} : pred) (args : args) =
    proof_clauses clauses args
  in
  proof_predicate (predicate_new_vars p) input_args;;
                  
  

let proof_interactive (prog : program) (pred : pred) (args : args) (number_to_name : (int, string) Hashtbl.t) =
  let ask_if_correct mgu =
    let rec print_mgu mgu =
      match mgu with
      | [] -> ()
      | [(i, t)] ->
         begin
         try
         let name = "X"(*Hashtbl.find number_to_name i*) in
         print_string name;
         print_char '=';
         print_term t number_to_name;
         print_newline ()
         with
         | Not_found -> ()
         end
      | (i, t) :: rest ->
         begin
         try
         let name = "X" (* Hashtbl.find number_to_name i*) in
         print_string name;
         print_char '=';
         print_term t number_to_name;
         print_newline ();
         print_mgu rest
         with
         | Not_found -> print_mgu rest
         end
    in
    print_mgu mgu;
    print_string "Ok ?: ";
    let line = read_line () in
    line = ".;;"
  in
  proof ~on_success:ask_if_correct pred args

let print_license () =
  print_endline "
    Procaml -- Prolog interpreter in Ocaml
    Copyright (C) 2016  Sebastian Sura

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>."
