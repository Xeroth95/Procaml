open Core;;


(**
    Try to find a proof of the predicate 'p' called
    with the arguments 'args'. On success it returns
    the resulting most general unifier and the
    backtrack tree, which can be used to find another proof
 *)
let proof (p : pred) (args : args) =
  let rec find_sol bt_stack =
    (* When this method is called we are in the following
     situation:
     + We did not find a proof yet
     + Our search went through a wrong 'path'
     On return we return the found mgu AND
     the rest of the backtrack tree *)
    try
    let (try_next, args) = Stack.pop bt_stack in
    let clauses = try_next.clauses in
    proof_clauses clauses bt_stack
    with
    | Stack.Empty -> None;
  and proof_clauses clauses bt_stack =
    match clauses with
    | [] ->

       
  let bt_stack = Stack.create () in
  Stack.push (p, args) bt_stack;
  find_sol bt_stack;;

