open Core

exception UnifierException;;
     
type unifier
val empty : unit -> unifier
val add_binding : unifier -> variable -> term -> unifier
val combine : unifier -> unifier -> unifier
val apply_to_term : unifier -> term -> term
val apply_to_body : unifier -> (pred * args) list -> (pred * args) list

