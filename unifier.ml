open Core

module VarMap = Map.Make (struct type t = variable let compare = compare end)
type unifier = term VarMap.t

let empty () =
  VarMap.empty;;

let add_binding unifier var binding =
  VarMap.add var binding unifier;;

let rec apply_to_term unifier term =
  match term with
  | Val v -> term
  | Fun (f, n, l) ->
     Fun (f, n, List.map (apply_to_term unifier) l)
  | Var i ->
     try
     VarMap.find i unifier
     with
     | Not_found -> term;;
     
let apply_to_body unifier body = [];; (* TODO *)
let combine basis unifier = [];; (* TODO *)

  
